mod pomo;

use pomo::{Config, Pomo, State};
use std::io::{stdout, Seek, Write};
use std::path::{Path, PathBuf};
use std::time::Duration;
use std::{env, fs::File};

use anyhow::Result;
use crossterm::{
    cursor::{Hide, MoveTo, Show},
    event::{self, poll, read, Event},
    execute,
    style::Print,
    terminal::{disable_raw_mode, enable_raw_mode, Clear, ClearType},
};
use getopts::{Matches, Options};

fn main() -> Result<()> {
    let args: Vec<String> = env::args().collect();
    let program = args[0].clone();

    let mut opts = Options::new();
    opts.optopt(
        "w",
        "work-minutes",
        "Set the duration of the work clock, default: 25",
        "WORK_MINUTES",
    );
    opts.optopt(
        "b",
        "break-minutes",
        "Set the duration of the break clock, default: 5",
        "BREAK_MINUTES",
    );
    opts.optopt(
        "n",
        "num",
        "Set the number of pomodoros, default: 4",
        "NUM_POMODOROS",
    );
    opts.optflag(
        "d",
        "detach",
        "Start pomodoro in the background. Check status with 'pomo stauts'",
    );
    opts.optopt(
        "a",
        "alert-command",
        "The command that should be run when a timer completes. This command is run with the user's env PATH.",
        "ALERT_CMD",
    );
    opts.optflag("h", "help", "print this help menu");

    let matches = match opts.parse(&args[1..]) {
        Ok(m) => m,
        Err(f) => {
            panic!("{}", f.to_string())
        }
    };

    if matches.opt_present("h") {
        print_usage(&program, opts);
        return Ok(());
    }

    match matches.free.get(0).map(|c| c.as_str()) {
        Some("start") => start(&matches),
        Some("reset") => reset(),
        Some("status") => status(),
        Some("next") => next(),
        Some("skip") => skip(),
        Some("shift-forward") => shift_forward(),
        Some("shift-backward") => shift_backward(),
        _ => {
            print_usage(&program, opts);
            Ok(())
        }
    }
}

fn start(matches: &Matches) -> Result<()> {
    let existing_config = load_state().ok().map(|p| p.config);

    let work_minutes = matches.opt_str("w").map(|w| w.parse::<u64>().unwrap());
    let break_minutes = matches.opt_str("b").map(|b| b.parse::<u64>().unwrap());
    let num_pomodoros = matches.opt_str("n").map(|n| n.parse::<usize>().unwrap());
    let alert_cmd = matches.opt_str("a");

    let config = Config {
        work_seconds: work_minutes.map(|m| m * 60).unwrap_or(
            existing_config
                .as_ref()
                .map(|c| c.work_seconds)
                .unwrap_or(25 * 60),
        ),
        break_seconds: break_minutes.map(|m| m * 60).unwrap_or(
            existing_config
                .as_ref()
                .map(|c| c.break_seconds)
                .unwrap_or(5 * 60),
        ),
        num_pomodoros: num_pomodoros.unwrap_or(
            existing_config
                .as_ref()
                .map(|c| c.num_pomodoros)
                .unwrap_or(4),
        ),
        alert_cmd: alert_cmd.or(existing_config.map(|c| c.alert_cmd).unwrap_or(None)),
    };

    match matches.opt_present("d") {
        true => pomo_background(config),
        false => pomo_interactive(config),
    }
}

fn reset() -> Result<()> {
    let mut pomo = load_state()?;
    pomo.reset();
    save_state(&pomo)?;

    Ok(())
}

fn status() -> Result<()> {
    let mut pomo = load_state()?;
    if pomo.tick() {
        save_state(&pomo)?;
        if let Some(cmd) = pomo.config.alert_cmd.as_ref() {
            alert_user(cmd)?;
        }
    }
    println!("{}", pomo);

    Ok(())
}

fn next() -> Result<()> {
    let mut pomo = load_state()?;

    if pomo.state.should_wait() {
        pomo.next();
        save_state(&pomo)?;
    } else {
        if pomo.tick() {
            save_state(&pomo)?;
        }
    }
    println!("{}", pomo);

    Ok(())
}

fn skip() -> Result<()> {
    let mut pomo = load_state()?;
    pomo.next();
    save_state(&pomo)?;
    println!("{}", pomo);

    Ok(())
}

fn shift_forward() -> Result<()> {
    let mut pomo = load_state()?;
    pomo.state.shift_timer_forward(Duration::from_secs(60));
    pomo.tick();
    save_state(&pomo)?;
    println!("{}", pomo);

    Ok(())
}

fn shift_backward() -> Result<()> {
    let mut pomo = load_state()?;
    pomo.state.shift_timer_backward(Duration::from_secs(60));
    pomo.tick();
    save_state(&pomo)?;
    println!("{}", pomo);

    Ok(())
}

fn pomo_interactive(config: Config) -> Result<()> {
    setup();
    event_loop(config).or_else(|e| {
        cleanup();
        Err(e)
    })?;
    cleanup();

    Ok(())
}

fn pomo_background(config: Config) -> Result<()> {
    let mut pomo = Pomo::new(config);
    pomo.next();

    let json: String = serde_json::to_string(&pomo)?;

    let state_path = get_state_file_path();
    match state_path.parent() {
        Some(config_dir) => {
            std::fs::create_dir_all(config_dir).unwrap();
        }
        None => {}
    };
    let mut file = File::create(state_path)?;
    file.write_all(json.as_bytes())?;

    println!("{}", pomo);

    Ok(())
}

fn event_loop(config: Config) -> Result<()> {
    let mut pomo = Pomo::new(config);
    loop {
        if poll(Duration::from_millis(250))? {
            match read()? {
                Event::Key(event::KeyEvent {
                    code: event::KeyCode::Char('c'),
                    modifiers: event::KeyModifiers::CONTROL,
                })
                | Event::Key(event::KeyEvent {
                    code: event::KeyCode::Char('q'),
                    modifiers: _,
                }) => {
                    break;
                }
                Event::Key(event::KeyEvent {
                    code: event::KeyCode::Enter,
                    modifiers: _,
                }) => {
                    if pomo.state.should_wait() {
                        pomo.next();
                    }
                }
                Event::Key(event::KeyEvent {
                    code: event::KeyCode::Char('r'),
                    modifiers: _,
                }) => {
                    pomo.state = State::Ready;
                }
                Event::Key(event::KeyEvent {
                    code: event::KeyCode::Char('s'),
                    modifiers: _,
                }) => {
                    pomo.next();
                }
                Event::Key(event::KeyEvent {
                    code: event::KeyCode::Char(' '),
                    modifiers: _,
                }) => {
                    pomo.state.toggle_pause();
                }
                Event::Key(event::KeyEvent {
                    code: event::KeyCode::Char('>'),
                    modifiers: _,
                }) => {
                    pomo.state.shift_timer_forward(Duration::from_secs(60));
                }
                Event::Key(event::KeyEvent {
                    code: event::KeyCode::Char('<'),
                    modifiers: _,
                }) => {
                    pomo.state.shift_timer_backward(Duration::from_secs(60));
                }
                _ => {}
            }
        }

        pomo.tick();
        render(&pomo, 0)?;
        if pomo.state == State::Done {
            break;
        }
    }

    Ok(())
}

fn setup() {
    enable_raw_mode().unwrap();
    execute!(stdout(), Hide, Clear(ClearType::All), MoveTo(0, 0),).unwrap();
}

fn cleanup() {
    disable_raw_mode().unwrap();
    execute!(stdout(), Show).unwrap();
    println!(""); // Be kind, write a newline
}

fn load_state() -> Result<Pomo> {
    let file = File::options()
        .read(true)
        .write(true)
        .open(get_state_file_path())?;

    let pomo = serde_json::from_reader(&file)?;
    Ok(pomo)
}

fn save_state(pomo: &Pomo) -> Result<()> {
    let mut file = File::options()
        .read(true)
        .write(true)
        .open(get_state_file_path())?;

    let json: String = serde_json::to_string(pomo)?;
    file.set_len(0)?;
    file.seek(std::io::SeekFrom::Start(0))?;
    file.write_all(json.as_bytes())?;

    Ok(())
}

fn get_state_file_path() -> PathBuf {
    home::home_dir()
        .map(|mut h| {
            h.push(".config");
            h.push("pomo");
            h.push("state.json");
            return h;
        })
        .unwrap_or(Path::new("pomo_state.json").to_path_buf())
}

fn render<S: std::fmt::Display>(msg: S, line: u16) -> Result<()> {
    execute!(
        stdout(),
        MoveTo(0, line),
        Clear(ClearType::CurrentLine),
        Print(msg),
    )?;

    Ok(())
}

fn alert_user(cmd: &str) -> Result<()> {
    std::process::Command::new(cmd)
        .env("PATH", std::env::var("PATH").unwrap_or("".into()))
        .spawn()?;

    Ok(())
}

fn print_usage(program: &str, opts: Options) {
    let brief = format!("Usage: {} [options] <COMMAND>", program);
    print!("{}", opts.usage(&brief));
    println!(
        r#"
pomo can run in either interactive or background mode. Use `start -d` to run in the background. Then use the following commands to interact:

COMMANDS
    start           start a new set with the provided options
    status          show the current status
    next            start the next timer at the end of work or break
    skip            skip to the end of the current timer
    reset           reset back to the initial idle state
    shift-forward   adjust the current timer forward by 1 minute
    shift-backward  adjust the current timer backward by 1 minute"#
    );
}
