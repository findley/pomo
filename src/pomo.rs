use serde::{Deserialize, Serialize};
use serde_millis;
use std::time::{Duration, Instant};

#[derive(Serialize, Deserialize, Debug)]
pub struct Pomo {
    pub config: Config,
    pub state: State,
    pub pom_num: usize,
}

#[derive(Serialize, Deserialize, Debug, Default)]
pub struct Config {
    pub work_seconds: u64,
    pub break_seconds: u64,
    pub num_pomodoros: usize,
    pub alert_cmd: Option<String>,
}

#[derive(Serialize, Deserialize, PartialEq, Debug, Default)]
#[serde(rename_all = "snake_case")]
pub enum State {
    #[default]
    Ready,
    Working {
        remaining: Duration,
        #[serde(with = "serde_millis")]
        last_tick: Option<Instant>,
    },
    TakeBreak,
    OnBreak {
        remaining: Duration,
        #[serde(with = "serde_millis")]
        last_tick: Option<Instant>,
    },
    BackToWork,
    Done,
}

impl Pomo {
    pub fn new(config: Config) -> Self {
        Self {
            config,
            state: State::Ready,
            pom_num: 0,
        }
    }

    pub fn next(&mut self) {
        self.state = match self.state {
            State::Ready => State::Working {
                remaining: Duration::from_secs(self.config.work_seconds),
                last_tick: Some(Instant::now()),
            },
            State::Working { .. } if self.pom_num + 1 == self.config.num_pomodoros => State::Done,
            State::Working { .. } => State::TakeBreak,
            State::TakeBreak => State::OnBreak {
                remaining: Duration::from_secs(self.config.break_seconds),
                last_tick: Some(Instant::now()),
            },
            State::OnBreak { .. } => State::BackToWork,
            State::BackToWork => {
                self.pom_num += 1;
                State::Working {
                    remaining: Duration::from_secs(self.config.work_seconds),
                    last_tick: Some(Instant::now()),
                }
            }
            State::Done => State::Done,
        };
    }

    pub fn tick(&mut self) -> bool {
        match &mut self.state {
            State::Working {
                remaining: d,
                last_tick: Some(t),
            }
            | State::OnBreak {
                remaining: d,
                last_tick: Some(t),
            } => {
                *d = d.saturating_sub(t.elapsed());
                *t = Instant::now();
                if d.is_zero() {
                    self.next();
                    return true;
                }
            }
            _ => {}
        }

        return false;
    }

    pub fn reset(&mut self) {
        self.state = State::Ready;
        self.pom_num = 0;
    }
}

impl State {
    pub fn should_wait(&self) -> bool {
        match self {
            State::Ready | State::TakeBreak | State::BackToWork => true,
            _ => false,
        }
    }

    pub fn shift_timer_forward(&mut self, shift_amount: Duration) {
        match self {
            State::Working {
                remaining: d,
                last_tick: _,
            }
            | State::OnBreak {
                remaining: d,
                last_tick: _,
            } => {
                *d = d.saturating_sub(shift_amount);
            }
            _ => {}
        }
    }

    pub fn shift_timer_backward(&mut self, shift_amount: Duration) {
        match self {
            State::Working {
                remaining: d,
                last_tick: _,
            }
            | State::OnBreak {
                remaining: d,
                last_tick: _,
            } => {
                *d = d.saturating_add(shift_amount);
            }
            _ => {}
        }
    }

    pub fn toggle_pause(&mut self) {
        match self {
            State::Working {
                remaining: _,
                last_tick: t,
            }
            | State::OnBreak {
                remaining: _,
                last_tick: t,
            } => {
                if t.is_some() {
                    *t = None;
                } else {
                    *t = Some(Instant::now());
                }
            }
            _ => {}
        }
    }
}

impl std::fmt::Display for Pomo {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let pomo_count = format!("({}/{})", self.pom_num + 1, self.config.num_pomodoros);

        match self.state {
            State::Ready => write!(f, "🍅 Time to Work"),
            State::Working {
                remaining: d,
                last_tick: t,
            } => {
                let emoji = t.map(|_| "📖").unwrap_or("⏸️");
                write!(
                    f,
                    "🍅 Working {} {} {}",
                    pomo_count,
                    duration_display(d),
                    emoji
                )
            }
            State::TakeBreak => write!(f, "🍅 Take a Break {} ⏰", pomo_count),
            State::OnBreak {
                remaining: d,
                last_tick: t,
            } => {
                let emoji = t.map(|_| "⚽").unwrap_or("⏸️");
                write!(
                    f,
                    "🍅 On Break {} {} {}",
                    pomo_count,
                    duration_display(d),
                    emoji
                )
            }
            State::BackToWork => write!(f, "🍅 Back to Work {} ⏰", pomo_count),
            State::Done => write!(f, "🍅 All Done! 🎉"),
        }
    }
}

fn duration_display(d: Duration) -> String {
    let s = d.as_secs();

    let seconds = s % 60;
    let minutes = (s / 60) % 60;
    let hours = (s / 60) / 60;

    if hours > 0 {
        format!(
            "{:02}:{:02}:{:02}",
            hours,
            minutes.to_string().as_str(),
            seconds
        )
    } else {
        format!("{:02}:{:02}", minutes, seconds)
    }
}
