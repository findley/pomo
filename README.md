# Pomo
A no frills CLI pomodoro timer. Works well with xmobar or other light weight
Linux bars.

<img src="screenshot.png" alt="screenshot"/>

## Install
```
cargo install --git https://gitlab.com/findley/pomo.git
```

## Usage

### Interactive
Start with a 25 minute work timer, and a 5 minute break timer (the defaults):
```bash
$ pomo start
```

Use `<Enter>` to continue to the next phase.\
Use `<Space>` to pause the timer.\
Use `>` and `<` to adjust the timer by 1 minute.

### Background
Use the `-d` flag to run in the background.
```bash
$ pomo start -d
```

Pass the path to a script that should be run each time a timer completes.
```bash
$ pomo -d -a myalert.sh
```

Here's an example alert script that shows a desktop notification with
notify-send, and plays a notification sound with mpv.
```bash
#!/bin/bash
notify-send -i ~/Pictures/tomato-timer.png Pomodoro 'Timer Complete!'
mpv --really-quiet --loop=1 ~/Music/noti.wav
```

Display the current state with the `status` command.
```bash
$ pomo status
```

Advance to the next phase with the `next` command. See the help menu for other
available commands
```bash
$ pomo next
```

## Help
```
Usage: pomo [options] <COMMAND>

Options:
    -w, --work-minutes WORK_MINUTES
                        Set the duration of the work clock, default: 25
    -b, --break-minutes BREAK_MINUTES
                        Set the duration of the break clock, default: 5
    -n, --num NUM_POMODOROS
                        Set the number of pomodoros, default: 4
    -d, --detach        Start pomodoro in the background. Check status with
                        'pomo stauts'
    -a, --alert-command ALERT_CMD
                        The command that should be run when a timer completes.
                        This command is run with the user's env PATH.
    -h, --help          print this help menu

pomo can run in either interactive or background mode. Use `start -d` to run in the background. Then use the following commands to interact:

COMMANDS
    start           start a new set with the provided options
    status          show the current status
    next            start the next timer at the end of work or break
    skip            skip to the end of the current timer
    reset           reset back to the initial idle state
    shift-forward   adjust the current timer forward by 1 minute
    shift-backward  adjust the current timer backward by 1 minute
```

## Example xmobar Usage
Here's how you could use pomo with xmobar
```
Config {
        ...
       , commands =
           [ Run Date "<fn=2>\xf133</fn>  %b %d %Y  %I:%M %p " "date" 20
           ...
           , Run Com "pomo" ["status"] "pomo" 10
           ]
       , sepChar = "%"
       , alignSep = "}{"
       , template = "<action=`pomo next` button=1><fc=#ff6c6b>%pomo%</fc></action><fc=#666666> | </fc><fc=#dc4ef5> %date% </fc>"
       }
```

Now you can interact by clicking the bar, or using the commands `pomo start -d`,
`pomo reset`, `pomo next`, etc.
